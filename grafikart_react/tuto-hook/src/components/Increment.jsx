import {useState} from 'react'

function useIncrement (initial, step) {

    const [count, setCount] = useState(initial)
    const increment = () => {
        setCount(c => c + step)
    }
    return [count, increment]
}

export default useIncrement;
