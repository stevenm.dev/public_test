import React, {useEffect, Fragment} from 'react';
import useIncrement from './Increment';
import useToggle from './Toggle';
import useAutoIncrement from './AutoIncrement';

function Counter () {
    const [count, increment] = useIncrement(0, 2)
    const [compteurVisible, toggleCompteur] = useToggle(true)
    const autoCount = useAutoIncrement(0, 10)

    // useEffect(() => {
    //     const timer = window.setInterval(() => {
    //         increment()
    //     }, 1000)
    //     return function () {
    //         clearInterval(timer)
    //     }
    // }, [])

    // useEffect(() => {
    //     document.title = 'Compteur ' + count
    // }, [count]) 


    return (
        <Fragment>
            <input type="checkbox" onChange={toggleCompteur} checked={compteurVisible} />
            <br />

            {compteurVisible && <button onClick={increment}>Incrémenter: {count}</button>}
            <br />
            
            <button>Auto Incrémentation {autoCount}</button>
        </Fragment>
    )       

}

export default Counter;