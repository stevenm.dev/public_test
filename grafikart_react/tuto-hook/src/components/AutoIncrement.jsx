import {useState, useEffect} from 'react';

function useAutoIncrement (initialValue = 0, step = 2) {
    const [count, setCount] = useState(initialValue)
    useEffect(function () {
        const timer = window.setInterval(function () {
            setCount(c => c + step)
        }, 1000)

        return function () {
            clearInterval(timer)
        }
    }, [])
    return count
}

export default useAutoIncrement;