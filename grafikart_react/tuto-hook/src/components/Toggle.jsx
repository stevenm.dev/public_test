import {useState} from 'react';

function useToggle (initialValue = true) {

    const [value, setValue] = useState(initialValue)
    const toggle = function () {
        setValue(v => !v)
    }
    return [value, toggle]

}

export default useToggle;