import app from 'firebase/app';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyAzL3HA8msdY8KM2q-ghCiHozO3DvCjuYI",
    authDomain: "marvel-quiz-16e52.firebaseapp.com",
    projectId: "marvel-quiz-16e52",
    storageBucket: "marvel-quiz-16e52.appspot.com",
    messagingSenderId: "655955717860",
    appId: "1:655955717860:web:854b8bd6f3f62b529e46f1"
  };


class Firebase {
    constructor() {
        app.initializeApp(config);
        this.auth = app.auth();
    }

    // inscription
    signupUser = (email, password) => {
        this.auth.createUserWithEmailAndPassword(email, password);
    }

    // connexion
    loginUser = (email, password) => {
        this.auth.signInWithEmailAndPassword(email, password);
    }

    // déconnexion
    signoutUser = () => {
        this.auth.signOut();
    }

    // Récupération Password
    passwordReset = email => {
        this.auth.sendPasswordResetEmail(email);
    }
}

export default Firebase;