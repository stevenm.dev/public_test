import React, {useState} from 'react';
import './App.css';
import Menu from './components/Menu';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Docs from './components/Docs';
import Tutorials from './components/Tutorials';
import Community from './components/Community';
import Profile from './components/Profile';
import ErrorPage from './components/ErrorPage';
import Home from './components/Home';

function App() {


  const [dark, setDark] = useState(false)

  const goDark = () => {
    setDark(!dark)

    if(!dark) {
      document.body.classList.add("bg-secondary")
    } else {
      document.body.classList.remove("bg-secondary")
    }
  }  

  const classBtnTheme = dark ? 'btn-light' : 'btn-dark';
  const textBtnTheme = dark ? 'Light Mod' : 'Dark Mod'


    return (
      <BrowserRouter>
      
        <Menu />
        <button className={`btn ${classBtnTheme} mt-3 m-lg-3 float-end`} onClick={goDark} >{textBtnTheme}</button>
  
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/Docs" component={Docs} />
          <Route path="/tutorial" component={Tutorials} />
          <Route path="/community" component={Community} />
          <Route path="/users/:profileId" component={Profile} />
          <Route component={ErrorPage} />
        </Switch>
        
      </BrowserRouter>
      
    );

  }

export default App;
