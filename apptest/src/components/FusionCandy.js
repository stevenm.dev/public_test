import React, { Component } from "react";
import FusionCharts from "fusioncharts";
import Charts from "fusioncharts/fusioncharts.charts";
import ReactFC from "react-fusioncharts";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";

ReactFC.fcRoot(FusionCharts, Charts, FusionTheme);

const chartConfigs = {
  type: "column2d",
  width: 600,
  height: 400,
  dataFormat: "json",
  dataSource: {
    "chart": {
      "caption": "Countries With Most Oil Reserves [2017-18]",
      "subCaption": "In MMbbl = One Million barrels",
      "xAxisName": "Country",
      "yAxisName": "Reserves (MMbbl)",
      "numberSuffix": "K",
      "theme": "candy"
    },
    "data": [
      {
        "label": "Venezuela",
        "value": "290"
      },
      {
        "label": "Saudi",
        "value": "260"
      },
      {
        "label": "Canada",
        "value": "180"
      },
      {
        "label": "Iran",
        "value": "140"
      },
      {
        "label": "Russia",
        "value": "115"
      },
      {
        "label": "UAE",
        "value": "100"
      },
      {
        "label": "US",
        "value": "30"
      },
      {
        "label": "China",
        "value": "30"
      }
    ]
  }
};

class ChartGauge extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message:
        "You will see a notifications here for the chart lifecycle events"
    };

    this.beforeDataUpdate = this.beforeDataUpdate.bind(this);
    this.dataUpdated = this.dataUpdated.bind(this);
    this.drawComplete = this.drawComplete.bind(this);
    this.renderComplete = this.renderComplete.bind(this);
  }

  // Callback handler for event 'beforeDataUpdate'.
  beforeDataUpdate() {
    this.state.message = [<strong>Status: </strong>, " beforeDataUpdate"];
  }

  // Callback handler for event 'dataUpdated'.
  dataUpdated() {
    let newMessage = this.state.message.slice();
    newMessage.push(", dataUpdated");
    this.setState({
      message: newMessage
    });
  }

  // Callback handler for event 'drawComplete'.
  drawComplete() {
    let newMessage = this.state.message.slice();
    newMessage.push(", drawComplete");
    this.setState({
      message: newMessage
    });
  }

  // Callback handler for event 'renderComplete'.
  renderComplete() {
    let newMessage = this.state.message.slice();
    newMessage.push(", renderComplete");
    this.setState({
      message: newMessage
    });
  }

  render() {
    return (
        <div className="mt-4 text-center">
            <div>
                <ReactFC
                {...chartConfigs}
                fcEvent-beforeDataUpdate={this.beforeDataUpdate}
                fcEvent-dataUpdated={this.dataUpdated}
                fcEvent-drawComplete={this.drawComplete}
                fcEvent-renderComplete={this.renderComplete}
                />
                <p style={{ padding: "10px", background: "#f5f2f0" }}>
                {this.state.message}
                </p>
            </div>
      </div>
    );
  }
}

export default ChartGauge;