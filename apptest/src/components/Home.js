import React from 'react';
import ChartMap from './FusionMap';

const Home = () => {
    return(
        <div className="container">
            <h1>Home</h1>

            <p className="text-center mt-4">Welcome to Eu ullamco veniam laborum sint consequat. Ex incididunt quis culpa est culpa aute. Dolor laboris anim dolore ea ut veniam consectetur magna eu amet.</p>

            <ChartMap />

        </div>
    )
}

export default Home;