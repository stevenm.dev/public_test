import React, { Component } from 'react';
import $ from 'jquery';

class Profile extends Component {

    state = {
        data: {}
    }

    componentDidMount() {
        const id = this.props.match.params.profileId;

        let payload = {
            token: "jpt6ykRS1zeBokyBPK2LlA",
            data: {
              name: "nameFirst",
              email: "internetEmail",
              phone: "phoneHome",
              _repeat: 10
            }
        };
        
        $.ajax({
          type: "POST",
          url: "https://app.fakejson.com/q",
          data: payload,
          success: function(resp) {
            console.log(resp.data);
            }
        });
    }
    
    render() {
        return(
            <div className="container mt-3">

                <div className="alert alert-warning alert-dismissible fade show mt-3" role="alert">
                    <strong>Oups !</strong> Problème avec $.ajax pour récupérer des data voir Profile.js
                    <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                <h1>Profile</h1>

                <ul className="list-group list-group-flush">
                    <li className="list-group-item"><strong>Name: </strong></li>
                    <li className="list-group-item"><strong>Email: </strong></li>
                    <li className="list-group-item"><strong>Phone: </strong></li>                    
                </ul>
            </div>
        )
    }
}

export default Profile;