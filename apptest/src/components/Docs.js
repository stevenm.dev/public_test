import React from 'react';
import Chart from './FusionGauge';
import ChartGauge from './FusionCandy';

const Docs = () => {
    return(
        <div className="container">
            <h1>Getting Started</h1>

            <Chart />

            <ChartGauge />

        </div>
    )
}

export default Docs;