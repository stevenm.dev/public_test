import React from 'react'
import { Link, NavLink } from 'react-router-dom';

function Menu() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            
            <Link className="navbar-brand" to="/">Home</Link>
            
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav ms-auto">
                    <li className="nav-item">
                        <NavLink className="nav-link" aria-current="page" to="/Docs">Docs</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/tutorial">Tutorial</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/community">Community</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/users/:profileId">Users</NavLink>
                    </li>                    
                </ul>
            </div>            
        </nav>
    )
}

export default Menu;
